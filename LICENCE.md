COPYRIGHT © 2015 Stéphane Adjemian

Except otherwise noted, the content of this folder is licensed under [Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/) licence.