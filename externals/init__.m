restoredefaultpath();

here = pwd();

if ~exist('use_dynare')
    use_dynare = false;
end

if use_dynare
    addpath([here '/dynare/matlab']);
else
    addpath([here '/matlab2tikz/src']);
    addpath([here '/dates/src']);
    addpath([here '/dseries/src']);
    addpath([here '/jsonlab']);
    initialize_dates_toolbox;
    initialize_dseries_toolbox;
end