\documentclass[10pt,slidestop]{beamer}

\usepackage{etex}

\usepackage{fourier-orns}
\usepackage{ccicons}
\usepackage{amssymb}
%\usepackage[centertags]{amsmath}
\usepackage{amstext}
\usepackage{amsbsy}
\usepackage{amsopn}
\usepackage{amscd}
\usepackage{amsxtra}
\usepackage{amsthm}
\usepackage{float}
\usepackage{color}
\usepackage{mathrsfs}
\usepackage{bm}
\usepackage{lastpage}
\usepackage[nice]{nicefrac}
\usepackage{setspace}
\usepackage{hyperref}
\usepackage{ragged2e}
\usepackage{listings}
\usepackage{algorithms/algorithm}
\usepackage{algorithms/algorithmic}


\usepackage{tikz,pgfplots}
\pgfplotsset{compat=newest}
\usetikzlibrary{patterns, arrows, decorations.pathreplacing, decorations.markings, calc, chains, shapes.arrows, fit}
\pgfplotsset{plot coordinates/math parser=false}
\newlength\figureheight
\newlength\figurewidth
\usepackage[utf8x]{inputenc}
\usepackage{cancel}
\usepackage{tikz-qtree}

\newcommand{\trace}{\mathrm{tr}}
\newcommand{\vect}{\mathrm{vec}}
\newcommand{\tracarg}[1]{\mathrm{tr}\left\{#1\right\}}
\newcommand{\vectarg}[1]{\mathrm{vec}\left(#1\right)}
\newcommand{\vecth}[1]{\mathrm{vech}\left(#1\right)}
\newcommand{\iid}[2]{\mathrm{iid}\left(#1,#2\right)}
\newcommand{\normal}[2]{\mathcal N\left(#1,#2\right)}
\newcommand{\dynare}{\href{http://www.dynare.org}{\color{blue}Dynare}}
\newcommand{\sample}{\mathcal Y_T}
\newcommand{\samplet}[1]{\mathcal Y_{#1}}
\newcommand{\slidetitle}[1]{\fancyhead[L]{\textsc{#1}}}
\newcommand{\red}{\color{red}}
\newcommand{\blue}{\color{blue}}


\definecolor{gray}{gray}{0.4}


\setbeamertemplate{footline}{
{\hfill\vspace*{1pt}\href{https://creativecommons.org/licenses/by-sa/4.0/legalcode}{\ccbysa}\hspace{.1cm}
\raisebox{-.05cm}{\href{https://gitlab.com/stepan-a-dynare/introduction-to-dynare.git}{\includegraphics[scale=.125]{../img/gitlab.png}}}
}\hspace{1cm}}

\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{blocks}[rounded][shadow=true]
\newenvironment{notes}
{\bgroup \justifying\bgroup\tiny\begin{spacing}{1.0}}
{\end{spacing}\egroup\egroup}

\newenvironment{exercise}[1]
{\bgroup \small\begin{block}{Ex. #1}}
{\end{block}\egroup}

\definecolor{arrowcolor}{RGB}{201,216,232}% color for the arrow filling
\definecolor{circlecolor}{RGB}{79,129,189}% color for the inner circles filling
\colorlet{textcolor}{white}% color for the text inside the circles
\colorlet{bordercolor}{white}% color for the outer border of circles

\pgfdeclarelayer{background}
\pgfsetlayers{background,main}

\newcounter{task}

\newlength\taskwidth% width of the box for the task description
\newlength\taskvsep% vertical distance between the task description and arrow

\setlength\taskwidth{2.5cm}
\setlength\taskvsep{17pt}

\def\taskpos{}
\def\taskanchor{}

\newcommand\task[1]{%
  {\parbox[t]{\taskwidth}{\scriptsize\Centering#1}}}

\tikzset{
inner/.style={
  on chain,
  circle,
  inner sep=4pt,
  fill=circlecolor,
  line width=1.5pt,
  draw=bordercolor,
  text width=1.2em,
  align=center,
  text height=1.25ex,
  text depth=0ex
},
on grid
}

\newcommand\Task[2][]{%
\node[inner xsep=0pt] (c1) {\phantom{A}};
\stepcounter{task}
\ifodd\thetask\relax
  \renewcommand\taskpos{\taskvsep}\renewcommand\taskanchor{south}
\else
  \renewcommand\taskpos{-\taskvsep}\renewcommand\taskanchor{north}
\fi
\node[inner,font=\footnotesize\sffamily\color{textcolor}]    
  (c\the\numexpr\value{task}+1\relax) {#1};
\node[anchor=\taskanchor,yshift=\taskpos] 
  at (c\the\numexpr\value{task}+1\relax) {\task{#2}};
}

\newcommand\drawarrow{% the arrow is placed in the background layer 
                                                     % after the node for the tasks have been placed
\ifnum\thetask=0\relax
  \node[on chain] (c1) {}; % if no \Task command is used, the arrow will be drawn
\fi
\node[on chain] (f) {};
\begin{pgfonlayer}{background}
\node[
  inner sep=10pt,
  single arrow,
  single arrow head extend=0.8cm,
  draw=none,
  fill=arrowcolor,
  fit= (c1) (f)
] (arrow) {};
\fill[white] % the decoration at the tail of the arrow
  (arrow.before tail) -- (c1|-arrow.west) -- (arrow.after tail) -- cycle;
\end{pgfonlayer}
}

\newenvironment{timeline}[1][node distance=.75\taskwidth]
  {\par\noindent\begin{tikzpicture}[start chain,#1]}
  {\drawarrow\end{tikzpicture}\par}



\begin{document}

\title{Dynare}
\author[S. Adjemian]{St\'ephane Adjemian}
\institute{\texttt{stephane.adjemian@univ-lemans.fr}}
\date{November, 2015}

\begin{frame}
  \titlepage{}
\end{frame}

\begin{frame}
    \frametitle{Outline}
    \tableofcontents
  \end{frame}

\section{Introduction}


\begin{frame}[label=RamseyModel]
\frametitle{Ramsey model}

\bigskip

\[
\begin{split}
  y_t &= k_t^{\alpha} \\
  y_t &= c_t + i_t \\
  k_{t+1} &= i_t + (1-\delta) k_t \\
  \frac{c_{t+1}}{c_t} &= \beta \left(\alpha k_{t+1}^{\alpha-1}+1-\delta\right)\\
\end{split}
\]

\bigskip


A nonlinear dynamic model with:
\begin{itemize}
\item backward \emph{and} forward terms (decisions depend on expectations).
\item uncertainty (future is not known for sure).
\end{itemize}

\bigskip

$\hookrightarrow$ \hyperlink{RamseyModelCode}{Dynare syntax}


\end{frame}

\begin{frame}
  \frametitle{Problems to be solved}
  
  \bigskip
  \bigskip

  \[
  \mathbb E \left[ f_\theta(y_{t-1}, y_{t}, y_{t+1}, \varepsilon_t) \Bigl| \Omega_t \right] = 0 \text{ for } t\in \mathbb T
  \]

  \bigskip

  with
  \begin{itemize}
    \item $y_t$ a vector of endogenous variables.
    \item $\varepsilon_t$ a vector of innovations.
    \item $f_\theta$ a continuous non linear function indexed by a vector of parameters $\theta$.
    \item $\Omega_t$ an information set (filtration, \emph{i.e.} $\Omega_t \subseteq \Omega_{t+s} \forall s\geq 0$).
    \item $\mathbb E [\bullet |\Omega_t]$ the conditional expectation operator.
    \item $\mathbb T$ a discrete time set (finite or not).
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Problems to be solved}
  \framesubtitle{Perfect foresight models}
  \begin{itemize}
  \item $\mathbb T$ is finite: $\{1,\dots,T\}$.
  \item $\Omega_t = \Omega_T$ for all $t$.
  \item $y_0$ and $y_{T+1}$ given.
  \end{itemize}
  \bigskip
  \[
  f_\theta(y_{t-1}, y_{t}, y_{t+1}, \varepsilon_t) = 0 \text{ for } t\in \{1,\dots,T\}
  \]
  \begin{center}
    with, for instance, $\varepsilon_1 \neq 0$ and $\varepsilon_t = 0\quad \forall t>1$
  \end{center}
  \bigskip
  \begin{itemize}
  \item The solution is a path for the endogenous variables.
  \item Because time begins at time $t=1$, the impulse $\epsilon_1$ is not expected.
  \item A priori a shock at time $t>1$ is expected (though with some work it is possible to implement unexpected impulses in $t>1$).
  \item Typical experience: How does the economy behave today if the government announce a VAT cut for next year? By how much the households will postpone consumption? 
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Problems to be solved}
  \framesubtitle{Stochastic models}
  \begin{itemize}
  \item $\mathbb T$ is not finite: $\mathbb N$ or $\mathbb Z$.
  \item $\Omega_t = \{y_{\underline{t-1}},\varepsilon_{\underline{t}}\}$ for all $t$, the parameterized model, $f_\theta$, and the invariant distribution of the innovations, $\varepsilon$, are also in the information set. 
  \end{itemize}
  \bigskip
  \[
  \mathbb E \left[ f_\theta(y_{t-1}, y_{t}, y_{t+1}, \varepsilon_t) \Bigl| \Omega_t \right] = 0
  \]
  \bigskip
  \begin{itemize}
  \item The solution is an invariant mapping between $y_t$ and $(y_{t-1}, \varepsilon_t)$.
  \item Future realizations of the impulses are unknown (but the distribution is known).
  \item With some work, it is possible to consider smaller information sets (limited rational expectation, learning).
  \item Typical experience: Does economic fuctuations have a cost? If the cycle is found to be costly, which policy could dampen the fluctuations?
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Problems to be solved}
  \framesubtitle{Statistical inference}
  \begin{itemize}
    \bigskip
  \item The parameters, $\theta$ and the variance of the innovations $\Sigma$, are supposed to be known to the agents in the economy (in RE or PF models, but not in a LRE or learning models)...
    \bigskip
  \item Economists may have some informations about these parameters, but they do not know them with certainty.
    \bigskip
  \item We need to estimate these parameters (Bayesian inference, or ML approach).
    \bigskip
  \item Also we need to compare the fit of different models.
  \end{itemize}
\end{frame}


\section{Perfect foresight models}

\begin{frame}
  \frametitle{Perfect foresight models}
  \begin{itemize}
  \item Suppose that $\varepsilon_t=0$ for all $t\geq1$.
  \item The initial state of the economy, $y_0$, is given.
  \item We define $y^{\star}$ as the steady state of the dynamical system for the endogenous variables:
    \[
    f_\theta \left(y^{\star},y^{\star},y^{\star},0\right) = 0
    \]
  \item [Example] Saddle path property in the \hyperlink{fig:ramsey}{Ramsey model}.
    \bigskip
  \item We assume that the economy reaches $y^{\star}$ in finite time, by imposing $y_{T+1} = y^{\star}$.
  \item We have boundary conditions and
    \[
    \begin{split}
      f_\theta(y_0, y_1, y_2, 0) &= 0\\
      f_\theta(y_{t-1}, y_t, y_{t+1}, 0) &= 0\text{ for all } t=2,\dots,T-1\\
      f_\theta(y_{T-1}, y_T, y^{\star}, 0) &= 0
    \end{split}
    \]
    $\Rightarrow$ $T \times n$ unknows and $T \times n$ equations!
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Perfect foresight models}
  \begin{itemize}
  \item We need to solve a huge system of nonlinear equations.
  \item A Newton algorithm is used.
  \item Let
    \[
    F(\mathbf Y) = 0
    \]
    be the stacked system of non linear equations.
  \item Set an initial guess $\mathbf Y^{(0)}$, usually the steady state:
    $y^{\star}\otimes \vec{e}_T$
  \item Update the solution paths, $\mathbf Y^{(i+1)}$ ($i=0,1, \dots$), by solving the
    following linear system of equations:
    \[
      F\left(\mathbf Y^{(i)}\right) + J_F\left(\mathbf
        Y^{(i)}\right)\left(\mathbf Y^{(i+1)}
        -\mathbf Y^{(i)}\right) = 0
    \]
    where $J_F\left(\mathbf Y\right) = \frac{\partial F(\mathbf
      Y)}{\partial \mathbf Y'}$ is the jacobian matrix of $F$.
  \item Stop the iterations if
    \[
    ||F\left(\mathbf Y^{(i)}\right)|| < \epsilon
    \]
  \item Different methods are available to solve the systems of linear
    equations (we do not need to explicitely inverse the jacobian matrix). 
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Perfect foresight models}
  \begin{itemize}

  \item The size of the jacobian is very large. If we have a model with
    $n=100$ endogenous variables and $T=400$, we must solve systems of
    $40000$ linear equations!

    \bigskip

  \item This jacobian matrix is sparse:
    {\tiny
      \[
    J_F(\mathbf Y) =
    \begin{pmatrix}
      f_{y}^1 & f_{y_{+}}^1 & 0 &  \dots & \dots & \dots & \dots &  0\\
      f_{y_{-}}^2 & f_{y}^2 & f_{y_{+}}^2 &  0 & \dots  & \dots & \dots & 0\\
      0 & f_{y_{-}}^3 & f_{y}^3 & f_{y_{+}}^2 & 0 & \dots & \dots & 0\\
     &  & \ddots & \ddots & \ddots & \ddots &   &   \\
    0 & \dots & \dots & 0 & f_{y_{-}}^{T-2} & f_{y}^{T-2} & f_{y_{+}}^{T-2} & 0 \\
    0 & \dots & \dots & \dots & 0 & f_{y_{-}}^{T-1} & f_{y}^{T-1} &
    f_{y_{+}}^{T-1} \\
    0 & \dots & \dots & \dots & \dots & 0 & f_{y_{-}}^{T} & f_{y}^{T}
    \end{pmatrix}
    \]}
    with $f_x^t = \frac{\partial F(\mathbf Y_t)}{\partial x'}$ for $x$
    equal to $y =y_t$, $y_{-}=y_{t-1}$, $y_{+}=y_{t+1}$

    \bigskip

    \item We have to exploit the sparsity when solving the systems of
      linear equations.
  \end{itemize}
\end{frame}


\section{Rational expectation models}

\begin{frame}
  \frametitle{Rational expectation models}
  \framesubtitle{General problem}
  \begin{itemize}
  \item We consider the following type of model:
    \[
    \mathbb E_t\left[f(y_{t+1},y_t,y_{t-1},u_t)\right]=0
    \]
    with:
    \[
    \begin{split}
      u_t &= \sigma \varepsilon_t\\
      \mathbb E[\varepsilon_t] &= 0\\
      \mathbb E[\varepsilon_t\varepsilon_t'] &= \Sigma_\epsilon
    \end{split}
    \]
    where $\sigma$ is a scale parameter, $\varepsilon$ is a vector of auxiliary random variables.

\bigskip

  \item \textbf{Assumption} $f:\quad \mathbb
  R^{3n+q}\rightarrow \mathbb R^{n}$ is a differentiable function in $\mathcal C^{k}$.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Rational expectation models}
  \framesubtitle{Solution}
  \begin{itemize}
   \item The solution is the \textcolor{red}{unknown} function ${\color{red} g}$:
     \[
     y_t = {\color{red} g}(y_{t-1},u_t,\sigma)
     \]
   \item Then, we have:
  \[
  \begin{split}
    y_{t+1} &= {\color{red} g}(y_t,u_{t+1},\sigma)\\
    &= {\color{red} g}({\color{red} g}(y_{t-1},u_t,\sigma),u_{t+1},\sigma)
  \end{split}
  \]
  \item So we can define:
    \[
    F_{\color{red} g}(y_{t-1},u_t,u_{t+1},\sigma) =
    f({\color{red} g}({\color{red} g}(y_{t-1},u_t,\sigma),u_{t+1},\sigma),{\color{red} g}(y_{t-1},u_t,\sigma),y_{t-1},u_t)
    \]
  \item And our problem can be restated as:
    \[
    \mathbb E_t\left[F_{\color{red} g}(y_{t-1},u_t,{\color{blue} u_{t+1}},\sigma)\right] = 0
    \]
  \item To solve the model we have to identify the unknown
    function ${\color{red} g}$ (solve a functional equation).
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Rational expectation models}
  \framesubtitle{First order approximation}
  \begin{itemize}

  \item Let $\hat y = y_{t-1} - \bar y$, $u=u_t$, ${\blue u_+=u_{t+1}}$,
    $f_{y_+}=\frac{\partial f}{\partial y_{t+1}}$, $f_{y}=\frac{\partial
      f}{\partial y_t}$, $f_{y_-}=\frac{\partial f}{\partial y_{t-1}}$,
    $f_{u}=\frac{\partial f}{\partial u_t}$, ${\red g_{y}}=\frac{\partial
      {\red g}}{\partial y_{t-1}}$, ${\red g_u}=\frac{\partial {\red g}}{\partial u_t}$,
    ${\red g_\sigma}=\frac{\partial {\red g}}{\partial \sigma}$.\newline Where all the
    derivates are evaluated at the deterministic steady state, $y^{\star}$.

    \bigskip

  \item With a first order Taylor expansion of $F$ around $y^{\star}$:
    \[
    \begin{split}
      0 &\simeq F_{\red g}^{(1)}(y_{-},u,{\blue u_{+}},\sigma) =\\
      &f_{y_+}\left({\red g_{y}}\left({\red g_{y}}\hat y +{\red g_u}u+{\red g_\sigma}\sigma\right)+{\red g_u}{\blue u_+}+
        {\red g_{\sigma}}\sigma\right)\\
      &+ f_{y}\left({\red g_{y}}\hat y +{\red g_u}u+{\red g_{\sigma}}\sigma\right)+f_{y_-}\hat y +f_u u
    \end{split}
    \]

    \bigskip

  \item \textbf{What has changed?} We now have three unknown ``parameters'' (${\red g_{y}}$, ${\red g_{u}}$ and ${\red g_{\sigma}}$)
    instead of an infinite number of parameters (function $\red g$).

  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Rational expectation models}
  \framesubtitle{First order approximation}
  \begin{itemize}
  \item Taking the expectation conditional on the information at time $t$,
    we have:
    \[
    \begin{split}
      0 \simeq &
      f_{y_+}\left({\red g_{y}}\left({\red g_{y}}\hat y +{\red g_u}u+{\red g_\sigma}\sigma\right)+{\red g_u}\mathbb E_t
        [{\blue u_+}]+{\red g_{\sigma}}\sigma\right)\\
      &+ f_{y}\left({\red g_{y}}\hat y +{\red g_u}u+{\red g_{\sigma}}\sigma\right)+f_{y_-}\hat y +f_u u
    \end{split}
    \]

    \bigskip

    \item Or equivalently:
      \[
      \begin{split}
        0 \simeq &
        \left(f_{y_+}{\red g_{y}g_{y}}+f_{y}{\red g_{y}}+f_{y_-}\right) \hat y + \left(f_{y_+}{\red g_yg_u}+f_y{\red g_u}+f_u\right)u\\
        &+ \left(f_{y_+}{\red g_yg_{\sigma}}+f_{y_+}{\red g_{\sigma}}+f_y{\red g_{\sigma}}\right)\sigma
      \end{split}
      \]

      \bigskip

    \item This ``equality'' must hold for any value of $(\hat y, u, \sigma)$, so
      that the terms between parenthesis must be zero. We have three
      (multivariate) equations and three (multivariate) unknowns:
      \[
      \begin{cases}
        0 &= f_{y_+}{\red g_{y}g_{y}}+f_{y}{\red g_{y}}+f_{y_-}\\
        0 &= f_{y_+}{\red g_yg_u}+f_y{\red g_u}+f_u\\
        0 &= f_{y_+}{\red g_yg_{\sigma}}+f_{y_+}{\red g_{\sigma}}+f_y{\red
          g_{\sigma}}
      \end{cases}
      \]
\end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Rational expectation models}
  \framesubtitle{First order approximation}
  \begin{itemize}

    \item We must have:
    \[
    \left(f_{y_+}{\red g_yg_y}+f_{y}{\red g_y}+f_{y_-}\right)\hat y=
    0\quad \forall \hat y
    \]

    \bigskip

    \item This is a quadratic equation, but the unknown is a matrix!
      It is generally impossible to solve this equation analytically
      as we would do for a univariate quadratic equation.

    \bigskip

    \item If we interpret ${\red g_y}$ as a lead operator, we can rewrite the
      equation as a second order recurrent equation:
      \[
      f_{y_+}\hat y_{t+1}+f_{y}\hat y_{t}+f_{y_-}\hat y_{t-1} = 0
      \]

      \bigskip

    \item For a given initial condition, $\hat y_{t-1}$, an infinity
      of paths $(\hat y_t, \hat y_{t+1})$ is solution of the second
      order recurrent equation.

      \bigskip

    \item[$\leftrightsquigarrow$] In the phase diagram of the \hyperlink{fig:ramsey}{Ramsey model}, an infinity of trajectories
      satisfy the Euler and transition equations.

  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Rational expectation models}
  \framesubtitle{First order approximation}
  \begin{itemize}

    \item [$\Rightarrow$] When we solve the quadratic equation for ${\red g_y}$, we must check that this allows to pin down a unique path for $(\hat y_t, \hat y_{t+1})$ leading to the steady state in the long run.

    \item To solve this quadratic equation we use a (real) generalized Schur decomposition (available in Lapack).

      \bigskip

    \item In the end we obtain the following reduced form:
      \[
      y_t = y^{\star}(\theta) + g_y(\theta) \left(y_{t-1}-y^{\star}\right) + g_u(\theta)\varepsilon_t
      \]

    \item The reduced form parameters are highly non linear function of the structural parameters $\theta$. In general we do not have closed form expressions for $g_y(\theta)$ and $g_u(\theta)$.

  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Rational expectation models}
  \framesubtitle{Higher order approximation}
  \begin{itemize}

    \item With a first order approximation, future uncertainty does not matter (certainty equivalence).

      \bigskip

    \item Dynare can compute the solutions of second and third order approximations of the original structural model.

      \bigskip

    \item Also, Dynare is distributed with a standalone program (dynare++, developped by Ondra Kamenik) which can solve a $k$-order approximation of the rational expectation model.
 

  \end{itemize}
\end{frame}


\section{Statistical inference}


\begin{frame}
  \frametitle{Bayesian inference}
  \framesubtitle{}
  \begin{itemize}

    \item Suppose that the economist has some beliefs about the parameters, and that these beliefs can be represented by a probability density function $p(\theta)$.

      \bigskip

    \item Suppose that the economist observe some variables. Let $\mathcal Z_T = \{z_1, z_2,\dots,z_T\}$ be the sample where $z_t$ is a subset of $y_t$. 

      \bigskip

    \item Is it possible to update the economist's beliefs with the data? How? 

  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Bayesian inference}
  \framesubtitle{}
  \begin{itemize}

    \item Any stochastic model defines implicitely a distribution for the endogenous variables.
      
      \bigskip

    \item For instance, the reduced form model:
      \[
      y_t = y^{\star}(\theta) + g_y(\theta) \left(y_{t-1}-y^{\star}\right) + g_u(\theta)\varepsilon_t
      \]
      implies that $y_t$ conditional on $y_{t-1}$ is Gaussian:
      \[
      y_t| y_{t-1} \sim \normal{\left(I_n-g_y(\theta)\right)y^{\star}(\theta)+g_y(\theta)y_{t-1}}{g_u(\theta)\Sigma g_u(\theta)'}
      \]
      provided that $\varepsilon_t\sim\normal{0}{\Sigma}$.
      
      \bigskip

    \item Obviously this implies that $(y_1,\dots,y_T)$ conditional on $y_0$ is also Gaussian. If $y_0$ is distributed as $y_{\infty}$, then $(y_1,\dots,y_T)$ is (marginally) Gaussian:
      \[
      (y_1,\dots,y_T)' \sim \normal{y^{\star}(\theta) \otimes e_T}{\Omega_y(\theta)}
      \]

  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Bayesian inference}
  \framesubtitle{}
  \begin{itemize}

  \item [$\Rightarrow$] Under the same assumptions the sample, $\mathcal Z_T$, is also Gaussian.
    \[
    (z_1,\dots,z_T)' \sim \normal{z^{\star}(\theta) \otimes e_T}{\Omega_z(\theta)}
    \]
    where $z^{\star} = S y^{\star}$ and $\Omega_z(\theta) = S \Omega_z(\theta) S'$, with $S$ a selection matrix.

    \bigskip

  \item The density of the sample is also called the \emph{likelihood}: 
    \[
    p(\mathcal Z_T| \theta) = (2\pi)^{-\frac{nT}{2}}|\Omega_z(\theta)|^{-\frac{1}{2}}e^{-\frac{1}{2}(z-z^{\star}(\theta)\otimes e_T)\Omega_z(\theta)^{-1}(z-z^{\star}(\theta)\otimes e_T)'}
    \]

    \bigskip

  \item Statistical inference $\Leftrightarrow$ Reverse the conditioning, we want to compute:
    \[
    p(\theta | \mathcal Z_t)
    \]
    what can we say about the parameters knowing the data?

  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Bayesian inference}
  \framesubtitle{}
  \begin{itemize}

  \item The well known Bayes theorem precisely establishes how to revert a conditioning.

    \bigskip

  \item Let $A$ and $B$ be two random variables.

    \bigskip

  \item We have:
    \[
    p(A|B) = \frac{p(A,B)}{p(B)}
    \]
    and also
    \[
    p(B|A) = \frac{p(A,B)}{p(A)}
    \]
    Substituting the second equation in the first one to eliminate the joint probability:
    \[
    p(A|B) = \frac{p(B|A)p(A)}{p(B)}
    \]
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Bayesian inference}
  \framesubtitle{}
  \begin{itemize}

  \item Applying this result to our inference problem, we obtain:
    \[
    \begin{split}
      p(\theta|\mathcal Z_T) &= \frac{p(\mathcal Z_T|\theta)p(\theta)}{p(\mathcal Z_T)}\\
      &\\
      \Rightarrow p(\theta|\mathcal Z_T) &\propto p(\mathcal Z_T|\theta)p(\theta)\\
    \end{split}
    \]
  \item The prior beliefs are updated by the data through the likelihood.
  \item The posterior density, \emph{ie} what we finally know about $\theta$, is proportional to the likelihood times the prior density.
  \item $p(\mathcal Z_t)$, the marginal density of the sample, is only used for model comparison.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Bayesian inference}
  \framesubtitle{}
  \begin{itemize}

    \bigskip

  \item Evaluation of the likelihood $\Rightarrow$ Kalman filters.

    \bigskip

  \item Estimation of the posterior mode $\Rightarrow$ Optimization routines.

    \bigskip

  \item Posterior moments and distribution $\Rightarrow$ Monte Carlo Markov Chains.

  \end{itemize}
\end{frame}


\section{Dynare}

\begin{frame}
  \frametitle{Dynare}
  \begin{itemize}
    \bigskip
  \item Dynare is a free Matlab/Octave toolbox that allows to
    \begin{itemize}
    \item Solve RE and PF models.
    \item Estimate and compare RE models.
    \item Characterize the design of optimal policies.
    \end{itemize}

    \bigskip
  \item Dynare comes also with a preprocessor which allows the user to write models in a natural manner and translate the work to be done in Matlab/Octave/C codes.

  \end{itemize}
\end{frame}


\begin{frame}[label=AugmentedRamseyModel]
\frametitle{Augmented Ramsey model}

\bigskip

\[
\begin{split}
  a_t &= \rho a_{t-1} + \varepsilon_t\\
  y_t &= e^{a_t}k_t^{\alpha} \\
  y_t &= c_t + i_t \\
  k_{t+1} &= i_t + (1-\delta) k_t \\
  \frac{c_{t+1}}{c_t} &= \beta \left(\alpha e^{a_{t+1}}k_{t+1}^{\alpha-1}+1-\delta\right)\\
\end{split}
\]

\bigskip
\bigskip

$\Rightarrow$ What is the effect of an expected temporary rise of productivity? 

\end{frame}


\begin{frame}[label=AugmentedRamseyModelCode1]
\frametitle{Augmented Ramsey model (Dynare code, I)}
  \lstinputlisting[firstline=1,lastline=18,frame=none,basicstyle=\footnotesize]{../mod/augmentedramsey.mod}
\end{frame}

\begin{frame}[label=AugmentedRamseyModelCode1]
\frametitle{Augmented Ramsey model (Dynare code, II)}
  \lstinputlisting[firstline=20,lastline=35,frame=none,basicstyle=\footnotesize]{../mod/augmentedramsey.mod}
\end{frame}


\begin{frame}[label=AugmentedRamseyModelResults]
\frametitle{Augmented Ramsey model (Productivity path)}
  \setlength\figureheight{6cm}
  \setlength\figurewidth{9cm}
  \begin{center}
  \input{a-ramsey.tex}
  \end{center}
\end{frame}

\begin{frame}[label=AugmentedRamseyModelResults]
\frametitle{Augmented Ramsey model (Consumption path)}
  \setlength\figureheight{6cm}
  \setlength\figurewidth{9cm}
  \begin{center}
  \input{c-ramsey.tex}
  \end{center}
\end{frame}

\begin{frame}[label=AugmentedRamseyModelResults]
\frametitle{Augmented Ramsey model (Output path)}
  \setlength\figureheight{6cm}
  \setlength\figurewidth{9cm}
  \begin{center}
  \input{y-ramsey.tex}
  \end{center}
\end{frame}


\begin{frame}[c]
  \frametitle{Dynare releases}
  \begin{center}
    \begin{timeline}
      \Task[v1]{Deterministic models\\ Gauss\\ 1994}
      \Task[v2]{Stochastic models\\ Matlab\\ 2002}
      \Task[v3]{Estimation\\ Matlab {\color{gray}+ Scilab}\\ 2004}
      \Task[v4]{C++ preprocessor\\ Matlab + Octave\\ 2008}
    \end{timeline}
  \end{center}
\end{frame}


\begin{frame}[c]
  \frametitle{Dynare events}
  \begin{itemize}
  \item Dynare summer school (Paris), each year since 2004.
    \bigskip
  \item Dynare conference (around the world), each year since 2005.
    \bigskip
  \item Workshops.
    \bigskip
  \item Courses (Universities, Central banks, ...).
  \end{itemize}
\end{frame}


\begin{frame}[c]
  \frametitle{Dynare on the web}
  \begin{itemize}
    \item \href{www.dynare.org}{www.dynare.org}
    \begin{itemize}
      \item News (releases, events, ...).
      \item Documentation.
      \item Downloads for Windows, OS X and Linux (source).
    \end{itemize}
    \bigskip
    \item \href{www.dynare.org/phpBB3}{Forums}
    \begin{itemize}
      \item More than 4000 users (number of spammers?).
      \item More than 18000 (true) posts.
    \end{itemize}
    \bigskip
    \item \href{https://www.dynare.org/DynareWiki}{Wiki}
    \bigskip
    \item \href{https://www.dynare.org/cgi-bin/mailman/listinfo}{Mailing list}.
    \bigskip
    \item Code sources are available on \href{https://github.com/DynareTeam}{Github}.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Dynare website traffic}
  \framesubtitle{Unique visitors per month (IP)}
  \setlength\figureheight{6cm}
  \setlength\figurewidth{9cm}
  \begin{center}
  \input{visitors.tex}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Dynare website traffic}
  \framesubtitle{Bandwidth per month (Go, nominal)}
  \setlength\figureheight{6cm}
  \setlength\figurewidth{9cm}
  \begin{center}
  \input{bandwidth.tex}
  \end{center}
\end{frame}





\begin{frame}[label=RamseyModelCode]
  \frametitle{\hyperlink{RamseyModel}{Ramsey model} (Dynare code, model declaration)}
  
  \lstinputlisting[firstline=1,lastline=21,frame=none,basicstyle=\footnotesize]{../mod/ramsey.mod}
  

\end{frame}


\begin{frame}
  \frametitle{\hyperlink{RamseyModel}{Ramsey model} (Dynare code, simulation)}
  
  \lstinputlisting[firstline=23,lastline=38,frame=none,basicstyle=\footnotesize]{../mod/ramsey.mod}
  

\end{frame}


\begin{frame}[label=RamseyModelPhaseDiagram]
  \frametitle{Ramsey model (phase diagram)}
  \label{fig:ramsey}
\begin{center}
\begin{tikzpicture}[scale = 0.6, transform shape]
    \draw[->] (0, 0) -- (12, 0) node [right] {$k_{t}$} ;
    \draw[->] (0, 0) -- (0, 12) node [left] {$c_t$} ;

    \draw[dashed,gray] (5,0) -- (5,7) ;
    \draw[dashed,gray] (0,7) -- (5,7) node[black] {$\bullet$};
    \node[below] at (5, 0) {$k^{\star}$} ;
    \node[left] at (0, 7) {$c^{\star}$} ;

    \draw[dashed,gray] (8,0) -- (8,7.5) ;
    \node[below] at (8, 0) {$\bar k$} ;

    \draw[thick] (0,0) to[out=80,in=200] (5,7) to[out=20,in=180] (8,7.5) to[out=0,in=95] (11.5,0) ;
    \draw[thick] (2,0) to[out=90*.86, in=-90*1.5] (5,7) to[out=90*.5,in=-90*1.6] (8,9.5);

    % Stable manifold--
    \draw[thick, red] (1.5,2) to[out=65,in=210] (5,7)
    [postaction={decorate, decoration={markings,mark=between positions 0.15 and 0.95 step 0.05 with 	{\arrow[red]{>};}}}];
    \draw[thick, red] (5, 7) to (9,9) [postaction={decorate, decoration={markings,mark=between positions 0.15 and 0.95 step 0.05 with 	{\arrow[red]{<};}}}];

    \draw[->] (1,1) -- (1,1.5) ;
    \draw[->] (1,1) -- (1.5,1) ;

    \draw[->] (10,9) -- (9.5,9) ;
    \draw[->] (10,9) -- (10,8.5) ;

    \draw[->] (7,3) -- (7.5,3) ;
    \draw[->] (7,3) -- (7,2.5) ;

    \draw[->] (2,9) -- (2,9.5) ;
    \draw[->] (2,9) -- (1.5,9) ;


    \draw[->] (2,1.5) to[out=60,in=180] (2.45,2) to[out=0,in =90] (2.9,1.5);
    \draw[->] (1.75,4) to[out=60,in=270] (2,4.94) to[out=90,in=315] (1.75,5.5);
    \draw[->] (8.6,8) to[out=200,in=90] (8.1,7.5) to[out=-90,in=135] (8.5,7) ;
    \draw[->] (7,8.4) to[out=240,in=0] (6.025,8) to[out=180,in=-20] (5,8.2);

    \draw[dashed,red]  (1.6,0) node[below] {$k_0$}-- (1.6,2.2) ;
    \draw[dashed,red] (0,2.2) node[left] {$c_0$} -- (1.6,2.2) ;
\end{tikzpicture}
\end{center}
\end{frame}

\end{document}

% Local Variables:
% ispell-check-comments: exclusive
% ispell-local-dictionary: "american-insane"
% End: