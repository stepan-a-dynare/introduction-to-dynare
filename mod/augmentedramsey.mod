var k y c i a;

varexo e;

parameters delta alpha beta rho;

delta = .02;
alpha = .33;
beta  = .99;
rho   = .50;

model;
    a = rho*a(-1)+e ;
    y = exp(a)*k(-1)^alpha ;
    i = y - c ;
    k = i + (1-delta)*k(-1) ;
    c(1)/c =  beta*(alpha*exp(a(1))*k^(alpha-1)+1-delta) ;
end;

steady_state_model;
    a = 0;
    k =  ( alpha/(1/beta-1+delta) )^(1/(1-alpha));
    y = k^alpha ;
    i = delta*k ;
    c = y - i ;
end;

steady;

shocks;
var e; periods 2; values .5;
end;

perfect_foresight_setup(periods=400);
perfect_foresight_solver;
