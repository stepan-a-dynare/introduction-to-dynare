var k y c i ;

parameters delta alpha beta ;

delta = .02;
alpha = .33;
beta  = .99;

model;
    y = k(-1)^alpha ;
    i = y - c ;
    k = i + (1-delta)*k(-1) ;
    c(1)/c =  beta*(alpha*k^(alpha-1)+1-delta) ;
end;

steady_state_model;
    k =  ( alpha/(1/beta-1+delta) )^(1/(1-alpha));
    y = k^alpha ;
    i = delta*k ;
    c = y - i ;
end;

kstar = ( alpha/(1/beta-1+delta) )^(1/(1-alpha));

initval;
    k = .5*kstar;
end;

endval; 
    k = kstar;
end;

steady;

perfect_foresight_setup(periods=200);
perfect_foresight_solver;

plot(y);
