To compile the slides you need first to clone this repository (with the submodules):
```bash
~$ git clone --recursive https://gitlab.com/stepan-a-dynare/introduction-to-dynare.git
```

[Dynare](http://www.dynare.org), used to produce some of the tikz figures included in the slides, comes as a submodule and needs to be compiled. You need to check [here](https://github.com/DynareTeam/dynare) that your platform has all the required dependencies to compile Dynare. The compilation can be triggered with the shell script provided in the base directory. Copy ```install-dynare.sh.example```:
```bash
~$ cp install-dynare.sh.example install-dynare.sh
```
Then some variables probably need to be changed for your system (```MATLAB_PATH```and ```MATLAB_VER```). Finallly you need to change the permissions on this file:
```bash
~$ chmod u+x install-dynare.sh
```
and run the script:
```bash
~$ ./install-dynare.sh
```

To compile the slides you need to use the Makefile in the ```tex``` subfolder (```matlab```needs to be in the system path, or you need to change the  ```MATLAB``` variable in this Makefile):
```bash
~$ cd tex
~$ make
```