function plot_ramsey_path(endovarname)
    use_dynare = false;
    origin = pwd();
    cd ../externals
    init__;
    cd(origin);
    load('augmentedramsey.mat')
    figure
    horizon = 40;
    idvar  = strmatch(endovarname, M_.endo_names, 'exact') 
    series = transpose(oo_.endo_simul(idvar,:));
    plot(series(2:horizon),'-k','linewidth',2);
    hold on
    plot([1 horizon-1],[oo_.steady_state(idvar) oo_.steady_state(idvar)],'-r','linewidth',2);
    plot([2, 2], [oo_.steady_state(idvar) max(series)],'-g')
    hold off
    axis tight
    ax = gca;
    ax.TickDir = 'out';
    matlab2tikz(['../tex/' endovarname '-ramsey.tex'], 'height', '\figureheight', 'width', '\figurewidth');
