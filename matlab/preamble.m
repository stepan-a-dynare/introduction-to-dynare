% Set paths
origin = pwd();
cd ../externals
init__;
cd(origin);

% Load Dynare's versions
release = loadjson('../data/dynare.releases.json');
v.names = fieldnames(release);
v.names = strrep(v.names, '_', '.');
v.names = strrep(v.names, 'v', '');
v.dates = getallfields(release);

% Load data
dynare = dseries('../data/dynare.statistics.csv');

% Get labels for the abscissa
t1 = find(dynare.dates==dates('2010M1'));
t2 = find(dynare.dates==dates('2011M1'));
t3 = find(dynare.dates==dates('2012M1'));
t4 = find(dynare.dates==dates('2013M1'));
t5 = find(dynare.dates==dates('2014M1'));
t6 = find(dynare.dates==dates('2015M1'));