figure()
hold on
for i=1:length(v.names)
    t0 = find(dynare.dates==dates(v.dates{i}));
    if ~isempty(t0)
        if isequal(v.names{i}(end-1:end),'.0')
            plot([t0 t0],[min(series) max(series)], '-r','linewidth', 2)
            vflag = text(t0+.5,min(series)+.975*(max(series)-min(series)),v.names{i}(1:3));
            vflag.FontWeight = 'bold';
            vflag.Color = 'red';
        else
            plot([t0 t0],[min(series) max(series)], '-r')
        end
    end
end
plot(series,'-k','linewidth', 2);
box on
axis tight
ax = gca;
ax.TickDir = 'out';
ax.XTick = [t1, t2, t3, t4, t5, t6];
ax.XTickLabel = {'2010', '2011', '2012', '2013', '2014', '2015'};
hold off