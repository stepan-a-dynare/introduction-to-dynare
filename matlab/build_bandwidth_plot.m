preamble;

series = dynare.data(:,end);

epilogue;

matlab2tikz('../tex/bandwidth.tex', 'height', '\figureheight', 'width', '\figurewidth');