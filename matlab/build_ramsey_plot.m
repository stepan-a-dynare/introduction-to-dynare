use_dynare= true;

opath = pwd();
cd ../externals
init__;
cd([opath '/../mod']);


dynare augmentedramsey


copyfile('augmentedramsey_results.mat','../tex/augmentedramsey.mat');
rmdir('augmentedramsey','s');
delete('augmentedramsey.m', 'augmentedramsey_dynamic.m',  'augmentedramsey_set_auxiliary_variables.m', 'augmentedramsey_steadystate2.m', 'augmentedramsey.log', 'augmentedramsey_static.m', 'augmentedramsey_results.mat');