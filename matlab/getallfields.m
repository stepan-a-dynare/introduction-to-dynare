function Cell = getallfields(Structure)
    names = fieldnames(Structure);
    Cell = cell(length(names), 1);
    for i=1:length(names)
        Cell(i) = { Structure.(names{i}) };
    end